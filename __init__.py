# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "GP_DOPESHEETS",
    "author" : "Tom VIGUIER",
    "description" : "See and edit grease pencil frames from regular dopesheet",
    "blender" : (2, 93, 0),
    "version" : (0, 0, 1),
    "location" : "Dopesheet's top right corner",
    "warning" : "EXPERIMENTAL TOOL",
    "category" : "Andarta"
}

import bpy
import rna_keymap_ui
from bpy.types import  (AddonPreferences,
                       PropertyGroup,
                       )

from bpy.props import *
from bpy.app.handlers import persistent


def update_autolock_layers(self, context) :
    window = bpy.context.window_manager
    displayed_objects = window.ds_data.filter_displayed_objects
    if self.autolock_layers == True :
        channels = get_selected_channels()
        if len(channels) > 0 :
            channel = channels[0]
            auto_lock_layers(displayed_objects, channel)
    else :
        for ob in displayed_objects :
            for chan in ob.animation_data.action.groups['Grease Pencil frames'].channels :  
                chan.lock = False
                toggle_layer_lock(ob, chan.data_path[2:-2], False)
                if (ob, chan.data_path[2:-2]) in window.ds_data.filter_locked_channels:
                    window.ds_data.filter_locked_channels.remove((ob, chan.data_path[2:-2]))
    return None

def update_isolate_layer(self, context) :
    window = bpy.context.window_manager
    displayed_objects = window.ds_data.filter_displayed_objects
    if self.isolate_layer == True :
        channels = get_selected_channels()
        if len(channels) > 0 :
            channel = channels[0]
            isolate_layer(displayed_objects, channel)
    else :
        for ob in displayed_objects :
            for chan in ob.animation_data.action.groups['Grease Pencil frames'].channels :  
                chan.mute = False
                toggle_layer_hide(ob, chan.data_path[2:-2], False)
                if (ob, chan.data_path[2:-2]) in window.ds_data.filter_mute_channels:
                    window.ds_data.filter_mute_channels.remove((ob, chan.data_path[2:-2]))     
    return None

def add_ds_kf(kf, fcurve, ob, layer, gp_frame):
    window = bpy.context.window_manager
    window.ds_keyframes.kf.append(kf) 
    window.ds_keyframes.channel.append(fcurve)
    window.ds_keyframes.object.append(ob)
    window.ds_keyframes.layer.append(layer)
    window.ds_keyframes.gp_frame.append(gp_frame)

def get_displayed_objects() : #list objects visible in the dopesheet
    screen = bpy.context.screen
    scene = bpy.context.scene
    window = bpy.context.window_manager
    window.ds_data.filter_displayed_objects.clear()
    for area in screen.areas :
        if area.spaces[0].type == 'DOPESHEET_EDITOR' and area.spaces[0].mode == 'DOPESHEET':
            if area.spaces[0].dopesheet.show_only_selected == True :
                #print(bpy.context.selected_objects)
                for obj in bpy.context.selected_objects :
                    if obj.type == 'GPENCIL' :
                        window.ds_data.filter_displayed_objects.append(obj)
            else :
                if area.spaces[0].dopesheet.show_hidden == False :
                    for obj in bpy.context.visible_objects : 
                        if obj.type == 'GPENCIL' :  
                            window.ds_data.filter_displayed_objects.append(obj)
                else :
                    for obj in scene.objects :
                        if obj.type == 'GPENCIL' :
                            window.ds_data.filter_displayed_objects.append(obj)
            break
    return window.ds_data.filter_displayed_objects
    
def filter() :
    window = bpy.context.window_manager   
    window.ds_keyframes.kf.clear()
    window.ds_keyframes.object.clear()
    window.ds_keyframes.channel.clear()
    window.ds_keyframes.gp_frame.clear()
    window.ds_keyframes.layer.clear()

    #for ob in window.ds_data.filter_displayed_objects :
    for ob in get_displayed_objects() :
        if ob.type == 'GPENCIL' and ob.animation_data :
            if ob.animation_data.action and 'Grease Pencil frames' in ob.animation_data.action.groups :
                #add_object = False #
                group = ob.animation_data.action.groups['Grease Pencil frames']
                for fcurve in group.channels:
                    #add_fcurve = False #
                    for kf in fcurve.keyframe_points :
                        if kf.select_control_point == True :  
                            layer = [layer for layer in ob.data.layers if layer.info == fcurve.data_path[2:-2]][0]

                            gp_frame_str_list = [ob['gp_targets'][key]  for key in ob['gp_targets'].keys() if int(key) == kf.co.y ]

                            if len(gp_frame_str_list) > 0 :
                                gp_frame_str = gp_frame_str_list[0]
                                gp_frame = [frame for frame in layer.frames if str(frame) == gp_frame_str][0]
                                #print('/*/*/*/', kf.co.x, kf.co.y)      
                            else : 
                                gp_frame = None                
                            add_ds_kf(kf, fcurve, ob, layer, gp_frame)

def get_ds_kf(i):    
    window = bpy.context.window_manager
    keyframe = window.ds_keyframes.kf[i]
    fcurve = window.ds_keyframes.channel[i]
    gp_frame = window.ds_keyframes.gp_frame[i]
    layer = window.ds_keyframes.layer[i]
    ob = window.ds_keyframes.object[i]
    return keyframe, fcurve, gp_frame, layer, ob

def get_selected_channels(): #list all slected gp animation channels (fcurves)
    result = []
    for ob in get_displayed_objects():
        if  ob.animation_data and ob.animation_data.action and 'Grease Pencil frames' in ob.animation_data.action.groups :
            group = ob.animation_data.action.groups['Grease Pencil frames']
            for channel in group.channels :
                if channel.select :
                    result.append((ob, channel.data_path[2:-2]))
    if len(result) > 0:
        return result
    else : 
        return [None]

def get_locked_channels() :#list locked animation channels (fcurves)
    #print('get_locked')
    result = []
    for ob in get_displayed_objects():
        if  ob.animation_data and ob.animation_data.action and 'Grease Pencil frames' in ob.animation_data.action.groups :
            group = ob.animation_data.action.groups['Grease Pencil frames']
            for channel in group.channels :
                if channel.lock :
                    result.append((ob, channel.data_path[2:-2]))
    #print('get_locked end')
    return result

def get_mute_channels() :#list muted animation channels (fcurves)
    result = []
    for ob in get_displayed_objects():
        if  ob.animation_data and ob.animation_data.action and 'Grease Pencil frames' in ob.animation_data.action.groups :
            group = ob.animation_data.action.groups['Grease Pencil frames']
            for channel in group.channels :
                if channel.mute :
                    result.append((ob, channel.data_path[2:-2]))
    return result

def toggle_layer_lock(ob, layer_name, value) :
    #print('toggle layer lock')
    for layer in ob.data.layers:
        if layer.info == layer_name :
            layer.lock = value
    #print('toggle layer lock - end')

def toggle_layer_hide(ob, layer_name, value) :
    for layer in ob.data.layers:
        if layer.info == layer_name :
            layer.hide = value

def select_gp_layer(ob, layer_name):
    #select a specific layer (args = ob: object, layer_name: string name of the layer.info)                            
    for layer in ob.data.layers:
        if layer.info == layer_name :
            ob.data.layers.active = layer
    if ob != bpy.context.view_layer.objects.active :
        dm = False
        if bpy.context.mode != 'OBJECT' :
            mode = bpy.context.mode
            bpy.ops.object.mode_set(mode = 'OBJECT')
            dm = True
        #unselect other objects  
        if bpy.context.area.type == 'DOPESHEET_EDITOR' and bpy.context.area.spaces[0].dopesheet.show_only_selected == False :                     
            for obj in bpy.context.selected_objects :
                obj.select_set(False)            
        #select object
        ob.select_set(True)
        bpy.context.view_layer.objects.active = ob
        if dm == True :
            bpy.ops.object.mode_set(mode = mode)

def auto_lock_layers(displayed_objects, channel) :
    window = bpy.context.window_manager
    for ob in displayed_objects :
        for chan in ob.animation_data.action.groups['Grease Pencil frames'].channels :
            if ob == channel[0] and chan.data_path[2:-2] == channel[1] :
                chan.lock = False
                toggle_layer_lock(ob, chan.data_path[2:-2], False)
                if (ob, chan.data_path[2:-2]) in window.ds_data.filter_locked_channels:
                    window.ds_data.filter_locked_channels.remove((ob, chan.data_path[2:-2]))
            else : 
                chan.lock = True
                toggle_layer_lock(ob, chan.data_path[2:-2], True)
                if (ob, chan.data_path[2:-2]) not in window.ds_data.filter_locked_channels:
                    window.ds_data.filter_locked_channels.append((ob, chan.data_path[2:-2]))
                                
def isolate_layer(displayed_objects, channel):
    window = bpy.context.window_manager
    for ob in displayed_objects :
        for chan in ob.animation_data.action.groups['Grease Pencil frames'].channels :
            if ob == channel[0] and chan.data_path[2:-2] == channel[1] :
                chan.mute = False
                toggle_layer_hide(ob, chan.data_path[2:-2], False)
                if (ob, chan.data_path[2:-2]) in window.ds_data.filter_mute_channels:
                    window.ds_data.filter_mute_channels.remove((ob, chan.data_path[2:-2]))
            else : 
                chan.mute = True
                toggle_layer_hide(ob, chan.data_path[2:-2], True)
                if chan not in window.ds_data.filter_mute_channels:
                    window.ds_data.filter_mute_channels.append((ob, chan.data_path[2:-2]))


def clear(): #clear variables and channels
    #print('+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+clear dopesheet')
    scene = bpy.context.scene
    window = bpy.context.window_manager
    window.ds_data.max_kf = 0
    for ob in scene.objects:
        if ob.type == 'GPENCIL' and ob.animation_data : 
            try :
                del ob['gp_targets']
                #print(ob.name, 'gp target deleted OK')
            except KeyError as keyerror:
                #print(ob.name, 'error while deleting gp_targets', keyerror)
                pass
            if ob.animation_data.action and len(ob.animation_data.action.groups) > 0 :
                grp = False
                for gr in ob.animation_data.action.groups :
                    if gr.name == 'Grease Pencil frames' :
                        grp = True
                if grp :
                    group = ob.animation_data.action.groups['Grease Pencil frames']
                    for fcurve in group.channels :
                        fcurve.lock = False
                        prop = fcurve.data_path[2:-2] 
                        #print('del', prop)                      
                        del ob[prop]
                        ob.animation_data.action.fcurves.remove(fcurve)
                    ob.animation_data.action.groups.remove(group)  
    #print('+-+-+-+-+-+clear end')



def dope_refresh(mode):
    #force gpencil frames to refresh indices
    #args : mode = dopesheet mode 'GPENCIL' or 'DOPESHEET' 
    try :
        area = bpy.context.screen.areas[0]
        context2 = bpy.context.copy()
        context2['area'] = area
        cur_area_type = area.type
        area.type = 'DOPESHEET_EDITOR'
        area.spaces[0].mode = mode
        bpy.ops.action.mirror(context2, type = 'XAXIS')
        bpy.ops.action.mirror(context2, type = 'XAXIS')
        area.type = cur_area_type   

    except TypeError :
        if bpy.context.object is None or (mode == 'GPENCIL' and bpy.context.object.type != 'GPENCIL'):
            return
        cur_areatype = str(bpy.context.area.type)
        bpy.context.area.type = 'DOPESHEET_EDITOR'
        cur_space_mode = str(bpy.context.area.spaces[0].mode)

        bpy.context.area.spaces[0].mode = mode
        cur_show_only_selected = bpy.context.area.spaces[0].dopesheet.show_only_selected
        bpy.context.area.spaces[0].dopesheet.show_only_selected = False

        bpy.ops.action.select_all(action='SELECT')
        bpy.ops.action.mirror(type = 'XAXIS')
        bpy.ops.action.mirror(type = 'XAXIS')
        
        bpy.context.area.spaces[0].dopesheet.show_only_selected = cur_show_only_selected
        bpy.context.area.spaces[0].mode = cur_space_mode
        bpy.context.area.type = cur_areatype


    #print('refreshed', mode)
def sync_channel_selection():
    #print('sync')
    window = bpy.context.window_manager
    
    displayed_objects = get_displayed_objects()
    if window.ds_data.last_filter_displayed_objects != displayed_objects :
        clear()
        generate()
        window.ds_data.last_filter_displayed_objects.clear()
        for ob in displayed_objects :
            window.ds_data.last_filter_displayed_objects.append(ob)

    else :
        locked_channels = get_locked_channels()
        #print('locked channels : ', locked_channels)
        #print( window.ds_data.filter_locked_channels)
        if window.ds_data.filter_locked_channels != locked_channels:
            for channel in [chan for chan in locked_channels if chan not in window.ds_data.filter_locked_channels] :                
                toggle_layer_lock(channel[0], channel[1], True)

            try :
                for channel in  [chan for chan in window.ds_data.filter_locked_channels if chan not in locked_channels]:           
                    toggle_layer_lock(channel[0], channel[1], False)
            except :
                pass

            window.ds_data.filter_locked_channels.clear()
            for channel in locked_channels :
                window.ds_data.filter_locked_channels.append(channel)

        mute_channels = get_mute_channels()
        if window.ds_data.filter_mute_channels != mute_channels:
            for channel in [chan for chan in mute_channels if chan not in window.ds_data.filter_mute_channels] :
                toggle_layer_hide(channel[0], channel[1], True)
            try :
                for channel in  [chan for chan in window.ds_data.filter_mute_channels if chan not in mute_channels]:           
                    toggle_layer_hide(channel[0], channel[1], False)
            except :
                pass

            window.ds_data.filter_mute_channels.clear()
            for channel in mute_channels :
                window.ds_data.filter_mute_channels.append(channel)

        selected_channels = get_selected_channels()
        if window.ds_data.selected_channels != selected_channels and selected_channels != [None] :

            channels = [chan for chan in selected_channels if chan not in window.ds_data.selected_channels]
            if len(channels) > 0:
                channel = [chan for chan in selected_channels if chan not in window.ds_data.selected_channels][0]
                select_gp_layer(channel[0], channel[1])

                if window.ds_data.autolock_layers :
                    auto_lock_layers(displayed_objects, channel)
                if window.ds_data.isolate_layer :                 
                    isolate_layer(displayed_objects, channel)

        window.ds_data.selected_channels.clear()    
        for channel2 in selected_channels :           
            window.ds_data.selected_channels.append(channel2)

    #print('sync_end')


def dopesheet_handler(): #secondary handler, syncs layer selection
    #print('DS-handler')
    window = bpy.context.window_manager
    area = bpy.context.area
    if window.ds_data.ds_handler_security == False :
        window.ds_data.ds_handler_security = True
        window.ds_data.gpds_handler_security = True

        if area.type == 'DOPESHEET_EDITOR' and area.spaces[0].mode == 'DOPESHEET' :
            #print('DS-handler*******************************')
            
            sync_channel_selection()
                
            #print('DS_HANLDER END')
        
        window.ds_data.gpds_handler_security = False
        window.ds_data.ds_handler_security = False

@persistent
def load_pre_handler(scene, context): #third handler : makes sure the add-on is off while opening a new file
    window = bpy.context.window_manager
    if window.ds_data.switch_on == True : 
        bpy.ops.dope.switch()

#undo handlers : disable addon while undoing actions
@persistent
def undo_pre_handler(scene, context) :
    window = bpy.context.window_manager
    if window.ds_data.switch_on :
        #print('UNDO_PRE+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')

        #store selected channels
        selected_chans = get_selected_channels()
        window.ds_data.selected_channels.clear()
        for chan in selected_chans :
            window.ds_data.selected_channels.append(chan) 
        #print('selected :')
        #print(window.ds_data.selected_channels)

        bpy.app.handlers.depsgraph_update_post.remove(gpds_handler)    
        bpy.types.SpaceDopeSheetEditor.draw_handler_remove(window.ds_data.draw_handler[0], 'WINDOW')    
        clear()
        window.ds_data.switch_on = False
        window.ds_data.active_undo_handler = True
        #bpy.ops.dope.switch()
        #print('undo pre end')

@persistent    
def undo_post_handler(scene, context) : 
    window = bpy.context.window_manager
    if window.ds_data.active_undo_handler == True :
        #print('UNDO_POST+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')

        bpy.app.handlers.depsgraph_update_post.append(gpds_handler)
        handler = bpy.types.SpaceDopeSheetEditor.draw_handler_add(dopesheet_handler, (), 'WINDOW', 'POST_PIXEL')
        
        window.ds_data.draw_handler.clear()
        window.ds_data.draw_handler.append(handler)
        window.ds_data.gpds_handler_security = True
        
        generate() #initializing

        #print('should reselect :')        
        #print(window.ds_data.selected_channels)

        if bpy.context.object.animation_data and bpy.context.object.animation_data.action and window.ds_data.selected_channels[0]:
            for fcurve in bpy.context.object.animation_data.action.fcurves :
                if fcurve.data_path ==  str('["' + window.ds_data.selected_channels[0][1] + '"]') :
                    fcurve.select = True
                else :
                    fcurve.select = False
        try:
            select_gp_layer(window.ds_data.selected_channels[0][0], window.ds_data.selected_channels[0][1])
        except :
            #print('error while selecting gp layer')
            pass

        window.ds_data.gpds_handler_security = False
        window.ds_data.switch_on = True
    
        #print('switch')
        #bpy.ops.dope.switch()
        window.ds_data.active_undo_handler = False
        #print('undo post end')
        

def gpds_handler(scene): #Main handler, used to refresh the dope sheet
    window = bpy.context.window_manager
    ops = window.operators
    #print('gpds handler---')
    if window.ds_data.gpds_handler_security == False :   
        window.ds_data.gpds_handler_security = True
        window.ds_data.ds_handler_security = True

        #print('gpds handler-+-+-+-+-+-+-+-+-+-+-+-+-+-+-')
        if len(ops) >= 1 and ops[-1] != window.ds_data.last_ops[0] :
            maintain(ops[-1].bl_idname)
            window.ds_data.last_ops.clear()
            window.ds_data.last_ops.append(ops[-1])
        window.ds_data.gpds_handler_security = False
        window.ds_data.ds_handler_security = False

def create_kf(ob, fcurve, gpframe) :
    window = bpy.context.window_manager
    new_kf = fcurve.keyframe_points.insert(gpframe.frame_number, float(window.ds_data.max_kf) )

    ob['gp_targets'][str(window.ds_data.max_kf)] = str(gpframe)
    window.ds_data.max_kf += 1
    new_kf.interpolation = 'CONSTANT'
    new_kf.handle_left_type = 'VECTOR'
    new_kf.handle_right_type = 'VECTOR'
    new_kf.select_control_point = False
    new_kf.select_left_handle = False
    new_kf.select_right_handle = False
    new_kf.type = gpframe.keyframe_type 

def maintain(cur_ops) :
    #print('***maintain function***', cur_ops)
    window = bpy.context.window_manager

    '''if cur_ops in ['ACTION_OT_clickselect', 'ANIM_OT_channels_click' ] :
        dopesheet_handler()'''

    if cur_ops in ['GPENCIL_OT_layer_move', 'GPENCIL_OT_layer_add', 'GPENCIL_OT_hide', 'GPENCIL_OT_reveal', 'GPENCIL_OT_lock_all', 'GPENCIL_OT_unlock_all', 'GPENCIL_OT_interpolate_sequence', 'GPENCIL_OT_interpolate', 'FORCE_REDRAW_KEEP_SELECTED'] :
        #print('!!!!!!!!!!!!!')
        selected_channel = get_selected_channels()[0]   
        #print('selected: ', selected_channel)
        clear()
        generate()  
        if selected_channel != None :
            for ob in window.ds_data.filter_displayed_objects :
                if ob == selected_channel[0]:
                    for channel in ob.animation_data.action.groups['Grease Pencil frames'].channels :
                        if channel.data_path[2:-2] == selected_channel[1] :
                            channel.select = True
                            select_gp_layer(selected_channel[0], selected_channel[1])
                            pass
                        else :
                            channel.select = False
        return 'REDRAW'

    if cur_ops in ['GPENCIL_OT_layer_remove', 'GPENCIL_OT_layer_duplicate_object', 'GPENCIL_OT_layer_duplicate', 'GPENCIL_OT_layer_merge', 'GPLIB_OT_importer', 'FORCE_REDRAW'] :
        #print('redraw+++++++++++++++++++++++++++++++++')
        clear()
        generate()  
        return 'REDRAW'
        
    if cur_ops == 'ACTION_OT_delete': 
        redraw = False
        #find deleted keyframes and delete corresponding frame 
        for ob in window.ds_data.filter_displayed_objects :
            gp = ob.data            
            for layer in gp.layers:
                prop_name = str('["' + layer.info + '"]')
                for gpframe in layer.frames :            
                    del_key = None
                    for key in ob['gp_targets'].keys() :
                        if str(gpframe) == ob['gp_targets'][key] :
                            try:
                                fcurve = [fc for fc in ob.animation_data.action.groups['Grease Pencil frames'].channels if fc.data_path == prop_name][0]
                                linked = False
                                for keyframe in fcurve.keyframe_points :
                                    if keyframe.co.y == int(key) :
                                        linked = True
                                if linked == False :                     
                                    layer.frames.remove(gpframe)
                                    del_key = key                                    
                            except : #last keyframe of the fcurve has been deleted
                                layer.clear()
                                redraw = True
                            break
                    
                    if del_key is not None :    
                        del ob['gp_targets'][del_key]    
        if redraw :
            clear()
            generate()
        else :      
            dope_refresh('GPENCIL')
        return

    if cur_ops in ['ACTION_OT_keyframe_insert', 'ACTION_OT_duplicate_move']:
        filter()
        for i in range(len(window.ds_keyframes.kf)):
            keyframe, fcurve, gp_frame, layer, ob = get_ds_kf(i)
            #print(keyframe, fcurve, gp_frame, layer, ob)
            
            #check for duplicated keyframes
            for keyframe2 in fcurve.keyframe_points :
                if keyframe.co.y == keyframe2.co.y and keyframe != keyframe2 :
                    keyframe2.co.y = float(window.ds_data.max_kf)
                    
                    keyframe2.interpolation = 'CONSTANT'
                    keyframe2.handle_left_type = 'VECTOR'
                    keyframe2.handle_right_type = 'VECTOR'

                    keyframe.interpolation = 'CONSTANT'
                    keyframe.handle_left_type = 'VECTOR'
                    keyframe.handle_right_type = 'VECTOR'
                
                    new_gpframe = layer.frames.copy(gp_frame)
                    if cur_ops == 'ACTION_OT_keyframe_insert' :
                        new_gpframe.keyframe_type = 'KEYFRAME'


                    new_gpframe.frame_number = gp_frame.frame_number

                    ob['gp_targets'][str(window.ds_data.max_kf)] = str(new_gpframe)
                    window.ds_data.max_kf += 1
                    break
        
        #dope_refresh('GPENCIL')

    if cur_ops in ['TRANSFORM_OT_transform', 'ACTION_OT_duplicate_move', 'ACTION_OT_keyframe_insert']:
        if cur_ops != 'ACTION_OT_duplicate_move':
            filter()
 
        for i in range(len(window.ds_keyframes.kf)):
            keyframe, fcurve, gp_frame, layer, ob = get_ds_kf(i)
            #sync keyframe position and type
            if gp_frame.frame_number != int(keyframe.co.x) : #the fcurve kf position in time is not the same as its linked gp kf (fc kf is moving)
                for frame in layer.frames :
                    if frame.frame_number == int(keyframe.co.x) : #already a gp frame on destination frame_number : avoid frame stacking
                        try :
                            del [ob['gp_targets'][key] for key in ob['gp_targets'].keys() if ob['gp_targets'][key] == str(frame)][0]  
                        finally :      
                            #print('avoided frame stacking on frame', frame.frame_number)
                            layer.frames.remove(frame)
                            
                        break
                gp_frame.frame_number = int(keyframe.co.x)
                #print('XXXXXXXXXX moving fcurve kf : gp kf should follow to : ', keyframe.co.x)

        dope_refresh('GPENCIL')

    if cur_ops == 'ACTION_OT_keyframe_type' :
        filter()
        for i in range(len(window.ds_keyframes.kf)):
            keyframe, fcurve, gp_frame, layer, ob = get_ds_kf(i)
            gp_frame.keyframe_type = keyframe.type

    #check for new gp frames
    ob = bpy.context.object
    if ob and ob.type == 'GPENCIL' and ob.animation_data and ob.animation_data.action and 'Grease Pencil frames' in [group.name for group in ob.animation_data.action.groups] :
        for layer in ob.data.layers :
            for gpframe in [frame for frame in layer.frames if frame.frame_number == bpy.context.scene.frame_current] : 
                if str(gpframe) not in ob['gp_targets'].values() :
                    fcurve = [fc for fc in ob.animation_data.action.groups['Grease Pencil frames'].channels if fc.data_path[2:-2] == layer.info][0]
                    create_kf(ob, fcurve, gpframe)
    #print('maintain end')

def generate():
    #print('***generate method***')
    for ob in get_displayed_objects() :
        gp = ob.data     
        ob['gp_targets'] = dict() 

        #CHeck for existing animation_data
        if ob.animation_data == None :
            ob.animation_data_create()   
        if ob.animation_data.action == None :
            ob.animation_data.action = bpy.data.actions.new(ob.name + '_Action')
        if 'Grease Pencil frames' in [group.name for group in ob.animation_data.action.groups] :
            group = ob.animation_data.action.groups['Grease Pencil frames']
        else :
            group = ob.animation_data.action.groups.new('Grease Pencil frames')

        group.show_expanded = True
        group.select = False

        #generate channels 
        #print('existing fcurves (should have been cleared..)')      
        for i, layer in enumerate(reversed(gp.layers)):
            prop_name = str('["' + layer.info + '"]')
            ob[layer.info] = -100
            
            for fcurve in [fcurve for fcurve in ob.animation_data.action.fcurves if fcurve.data_path == prop_name] :
                fcurve.lock = False

            ob.keyframe_insert(prop_name, index=-1, frame= -100, group = group.name)

            for fcurve in group.channels :
                if fcurve.data_path == prop_name:      

                    #sync layer locks                         
                    fcurve.lock = layer.lock
                    fcurve.mute = layer.hide

                    if ob == bpy.context.object and layer.select :
                        #print('***', layer.info)
                        fcurve.select = True
                    else : 
                        fcurve.select = False
                    
                    for gpframe in layer.frames : 
                        if gpframe not in ob['gp_targets'].values() :           
                            create_kf(ob, fcurve, gpframe)
                    break

        for fcurve in group.channels :            
            for kf in fcurve.keyframe_points : #remove the initial keyframe 
                if kf.co.y == -100 :
                    fcurve.keyframe_points.remove(kf) 
    dope_refresh('DOPESHEET')
    bpy.context.view_layer.update()
    #print('generate END')
    
def draw(self, context):
    st = context.space_data
    ob = bpy.context.object
    if st.mode == 'DOPESHEET' :

        scene = context.scene
        tool_settings = context.tool_settings
        screen = context.screen

        preferences = context.preferences
        addon_prefs = addon_prefs = preferences.addons[__name__].preferences

        if addon_prefs.add_timeline_tools :
            self.layout.separator_spacer()
            row = self.layout.row(align=True)
            row.prop(tool_settings, "use_keyframe_insert_auto", text="", toggle=True)
            sub = row.row(align=True)
            sub.active = tool_settings.use_keyframe_insert_auto
            sub.popover(
                panel="TIME_PT_auto_keyframing",
                text="",
            )

            row = self.layout.row(align=True)
            row.operator("screen.frame_jump", text="", icon='REW').end = False
            row.operator("screen.keyframe_jump", text="", icon='PREV_KEYFRAME').next = False
            if not screen.is_animation_playing:
                # if using JACK and A/V sync:
                #   hide the play-reversed button
                #   since JACK transport doesn't support reversed playback
                if scene.sync_mode == 'AUDIO_SYNC' and context.preferences.system.audio_device == 'JACK':
                    row.scale_x = 2
                    row.operator("screen.animation_play", text="", icon='PLAY')
                    row.scale_x = 1
                else:
                    row.operator("screen.animation_play", text="", icon='PLAY_REVERSE').reverse = True
                    row.operator("screen.animation_play", text="", icon='PLAY')
            else:
                row.scale_x = 2
                row.operator("screen.animation_play", text="", icon='PAUSE')
                row.scale_x = 1
            row.operator("screen.keyframe_jump", text="", icon='NEXT_KEYFRAME').next = True
            row.operator("screen.frame_jump", text="", icon='FF').end = True
            self.layout.separator_spacer()

            row = self.layout.row()
            if scene.show_subframe:
                row.scale_x = 1.15
                row.prop(scene, "frame_float", text="")
            else:
                row.scale_x = 0.95
                row.prop(scene, "frame_current", text="")

            row = self.layout.row(align=True)
            row.prop(scene, "use_preview_range", text="", toggle=True)
            sub = row.row(align=True)
            sub.scale_x = 0.8
            if not scene.use_preview_range:
                sub.prop(scene, "frame_start", text="Start")
                sub.prop(scene, "frame_end", text="End")
            else:
                sub.prop(scene, "frame_preview_start", text="Start")
                sub.prop(scene, "frame_preview_end", text="End")
            self.layout.separator_spacer()

        if bpy.context.window_manager.ds_data.switch_on == True :
            #selected = st.dopesheet.show_only_selected
            enable_but = len(bpy.context.window_manager.ds_data.selected_channels) == 1

            row = self.layout.row(align=True)
            row.enabled = enable_but
            row.operator("gpencil.layer_add", icon='ADD', text="")
            row.operator("gpencil.layer_remove", icon='REMOVE', text="")
            row.menu("GPENCIL_MT_layer_context_menu", icon='DOWNARROW_HLT', text="")

            row = self.layout.row(align=True)
            row.enabled = enable_but
            row.operator("gpencil.layer_move", icon='TRIA_UP', text="").type = 'UP'
            row.operator("gpencil.layer_move", icon='TRIA_DOWN', text="").type = 'DOWN'
            
            row = self.layout.row(align=True)
            row.prop(bpy.context.window_manager.ds_data, "isolate_layer", text='', icon = 'RESTRICT_VIEW_ON')
            if bpy.context.window_manager.ds_data.autolock_layers : lock_icon = 'LOCKED' 
            else : lock_icon = 'UNLOCKED'
            row.prop(bpy.context.window_manager.ds_data, "autolock_layers", text='', icon = lock_icon)

        self.layout.operator(DS_OT_SWITCH.bl_idname, text="", icon = 'OUTLINER_DATA_GREASEPENCIL', depress=bpy.context.window_manager.ds_data.switch_on )

class DS_OT_SWITCH(bpy.types.Operator) :
    bl_label = "toggle grease pencil frames in regular dopesheet "
    bl_idname = 'dope.switch'
    #bl_options = {'UNDO'}       
    
    def execute(self, context):
        #print('ops.switch')       
        window = bpy.context.window_manager
        # #print(draw_handler)
        
        if window.ds_data.switch_on == True :                       
            #print('turn off')
            #remove handlers
            try :
                bpy.app.handlers.depsgraph_update_post.remove(gpds_handler)    
                bpy.app.handlers.undo_pre.remove(undo_pre_handler)
                bpy.app.handlers.undo_post.remove(undo_post_handler)
                bpy.app.handlers.redo_pre.remove(undo_pre_handler)
                bpy.app.handlers.redo_post.remove(undo_post_handler)
                #bpy.types.SpaceDopeSheetEditor.draw_handler_remove(window.ds_data.draw_handler[0], 'WINDOW')
                bpy.types.SpaceDopeSheetEditor.draw_handler_remove(window.ds_data.draw_handler[0], 'WINDOW')
            
            except :
                bpy.app.handlers.depsgraph_update_post.clear()
                bpy.app.handlers.undo_pre.clear()
                bpy.app.handlers.undo_post.clear()
                bpy.app.handlers.redo_pre.clear()
                bpy.app.handlers.redo_post.clear()
                for handler in window.ds_data.draw_handler :
                    #bpy.types.SpaceDopeSheetEditor.draw_handler_remove(handler, 'WINDOW')
                    bpy.types.SpaceDopeSheetEditor.draw_handler_remove(handler, 'WINDOW')
            finally :
                clear()
                window.ds_data.switch_on = False
            
        else :
            #print('turn on')            
            #enable handlers
            bpy.app.handlers.depsgraph_update_post.append(gpds_handler)
            bpy.app.handlers.undo_pre.append(undo_pre_handler)
            bpy.app.handlers.undo_post.append(undo_post_handler)
            bpy.app.handlers.redo_pre.append(undo_pre_handler)
            bpy.app.handlers.redo_post.append(undo_post_handler)
            
            handler = bpy.types.SpaceDopeSheetEditor.draw_handler_add(dopesheet_handler, (), 'WINDOW', 'POST_PIXEL')
            if window.ds_data.draw_handler :
                window.ds_data.draw_handler.clear()
            window.ds_data.draw_handler.append(handler)

            window.ds_data.gpds_handler_security = True

            generate() #initializing
            window.ds_data.gpds_handler_security = False
            window.ds_data.switch_on = True
        #print('ops.switch end')
        return {'FINISHED'}


class DS_PROPS(PropertyGroup):
    bl_label = "gp dopesheet properties"
    bl_idname = "ds.properties"
   
    expand_group : bpy.props.BoolProperty(default = False)
    switch_on : bpy.props.BoolProperty(default = False)
    moving : bpy.props.BoolProperty(default = False)
    
    active_undo_handler : bpy.props.BoolProperty(default = False)
    selected_channels = [] #previous selection state 
    selected_objects = [] #previous selection state 
    active_object = []
    ds_handler_security : bpy.props.BoolProperty(default = False)
    gpds_handler_security : bpy.props.BoolProperty(default = False)
    #status : bpy.props.BoolProperty(default = False)


    filter_displayed_objects = [] #objects displayed in the dope sheet
    last_filter_displayed_objects = []
    
    filter_locked_channels = []
    filter_mute_channels = []

    draw_handler = []
    last_ops = [None]
    max_kf : bpy.props.IntProperty(default = 0)

    autolock_layers : bpy.props.BoolProperty(default = False, name = 'autolock layers', update = update_autolock_layers)
    isolate_layer : bpy.props.BoolProperty(default = False, name = 'isolate active layer', update = update_isolate_layer)

class DS_KF(PropertyGroup):
    bl_label = "gp dopesheet filtered keyframes"
    bl_idname = "ds.kf"

    kf = []
    object = []
    channel = []
    gp_frame = []
    layer = []

class DS_OT_ADD_EXPOSURE(bpy.types.Operator) :
    bl_label = "add 1 frame exposure to selected keyframes "
    bl_idname = 'dope.add_exposure'
    bl_options = {'UNDO'}   

    def execute(self, context):
        area = bpy.context.area
        window = bpy.context.window_manager
        filter()
        objects = dict()
        #print(area.spaces[0].type, area.spaces[0].mode, window.ds_data.switch_on)
        if area.spaces[0].type == 'DOPESHEET_EDITOR' and area.spaces[0].mode == 'DOPESHEET' and window.ds_data.switch_on == True :
            for i in range(len(window.ds_keyframes.kf)):
                keyframe, fcurve, gp_frame, layer, ob = get_ds_kf(i)
                for kf in [kf for kf in fcurve.keyframe_points if kf.co.x > keyframe.co.x]:
                    kf.co.x += 1.0
                for gpfr in [fr for fr in layer.frames if fr.frame_number > gp_frame.frame_number]:
                    gpfr.frame_number += 1

                if ob not in objects :
                    objects[ob] = dict()
                    objects[ob][keyframe.co.x] = 1
                else :
                    if keyframe.co.x in objects[ob] :
                        objects[ob][keyframe.co.x] += 1
                    else : 
                        objects[ob][keyframe.co.x] = 1

            for obj in objects:
                for t in objects[obj] :
                    if objects[obj][t] == len(obj.data.layers):
                        for group in obj.animation_data.action.groups :
                            if group.name != 'Grease Pencil frames' :
                                for fcurve in group.channels :
                                    for n, kf in enumerate(fcurve.keyframe_points) :
                                        if kf.co.x > t and kf.co.x - fcurve.keyframe_points[n-1].co.x > 1.0:
                                            kf.co.x += 1.0    
            dope_refresh('GPENCIL')
            return{'FINISHED'}
        else :
            return {'CANCELLED'}

class DS_OT_REMOVE_EXPOSURE(bpy.types.Operator) :
    bl_label = "remove 1 frame exposure to selected keyframes "
    bl_idname = 'dope.remove_exposure'
    bl_options = {'UNDO'}   

    def execute(self, context):
        area = bpy.context.area
        window = bpy.context.window_manager
        filter()
        #print(area.spaces[0].type, area.spaces[0].mode, window.ds_data.switch_on, len(window.ds_keyframes.kf))
        if area.spaces[0].type == 'DOPESHEET_EDITOR' and area.spaces[0].mode == 'DOPESHEET' and window.ds_data.switch_on == True :
            
            objects = dict()
            
            for i in range(len(window.ds_keyframes.kf)):
                keyframe, fcurve, gp_frame, layer, ob = get_ds_kf(i)
                for n, kf in enumerate(fcurve.keyframe_points) :
                    if kf.co.x > keyframe.co.x and kf.co.x - fcurve.keyframe_points[n-1].co.x > 1.0:
                        kf.co.x -= 1.0
                for n, gpfr in enumerate(layer.frames):
                    if gpfr.frame_number > gp_frame.frame_number and gpfr.frame_number - layer.frames[n-1].frame_number > 1:
                        gpfr.frame_number -= 1
                 
                if ob not in objects :
                    objects[ob] = dict()
                    objects[ob][keyframe.co.x] = 1
                else :
                    if keyframe.co.x in objects[ob] :
                        objects[ob][keyframe.co.x] += 1
                    else : 
                        objects[ob][keyframe.co.x] = 1

            for obj in objects:
                for t in objects[obj] :
                    if objects[obj][t] == len(obj.data.layers):
                        for group in obj.animation_data.action.groups :
                            if group.name != 'Grease Pencil frames' :
                                for fcurve in group.channels :
                                    for n, kf in enumerate(fcurve.keyframe_points) :
                                        if kf.co.x > t and kf.co.x - fcurve.keyframe_points[n-1].co.x > 1.0:
                                            kf.co.x -= 1.0
            dope_refresh('GPENCIL')
            return{'FINISHED'}
        else :
            return {'CANCELLED'}

class GP_DV_AddonPref(AddonPreferences):
    bl_idname = __name__
 
    add_timeline_tools : bpy.props.BoolProperty(default = False)

    def draw(self, context):
        layout = self.layout
        layout.prop( self, 'add_timeline_tools', text = 'Add timeline tools to the Dopesheet' )
        layout.label(text="KeyMap :")
    
        col = layout.column()
        kc = bpy.context.window_manager.keyconfigs.addon
        for km, kmi in addon_keymaps:
            km = km.active()
            col.context_pointer_set("keymap", km)
            rna_keymap_ui.draw_kmi([], kc, km, kmi, col, 0)

class DS_OT_MAINTAIN(bpy.types.Operator) :
    bl_label = "sync GP dopesheet "
    bl_idname = 'dope.maintain'
    #bl_options = {'UNDO'}   
    order : bpy.props.StringProperty(default = 'FORCE_REDRAW')

    def execute(self, context):
        #print('force DS maintain')
        maintain(self.order)
        return {'FINISHED'}


addon_keymaps = []            
classes = [DS_PROPS, DS_OT_SWITCH, DS_KF, DS_OT_ADD_EXPOSURE, DS_OT_REMOVE_EXPOSURE, GP_DV_AddonPref, DS_OT_MAINTAIN ]

def register():
    for cls in classes :
        bpy.utils.register_class(cls)   
    bpy.types.WindowManager.ds_keyframes = bpy.props.PointerProperty(type = DS_KF) 
    bpy.types.DOPESHEET_HT_header.append(draw)
    bpy.types.WindowManager.ds_data = bpy.props.PointerProperty(type=DS_PROPS)
    bpy.app.handlers.load_pre.append(load_pre_handler)  
    #bpy.app.handlers.undo_pre.append(undo_pre_handler)
    #bpy.app.handlers.undo_post.append(undo_post_handler)
    #bpy.app.handlers.redo_pre.append(undo_pre_handler)
    #bpy.app.handlers.redo_post.append(undo_post_handler)
    bpy.app.handlers.save_pre.append(undo_pre_handler)  
    bpy.app.handlers.save_post.append(undo_post_handler)  

    # Keymapping
    kc = bpy.context.window_manager.keyconfigs.addon
    if kc :
        km = kc.keymaps.new(name="Dopesheet", space_type='DOPESHEET_EDITOR')
        kmi = km.keymap_items.new("dope.add_exposure", type = 'F5', value = 'PRESS')
        kmi.active = True
        addon_keymaps.append((km, kmi))

        kmi = km.keymap_items.new("dope.remove_exposure", type = 'F5', value = 'PRESS', shift = True)
        kmi.active = True
        addon_keymaps.append((km, kmi))
 
def unregister():
    for cls in classes :
        bpy.utils.unregister_class(cls)
    bpy.types.DOPESHEET_HT_header.remove(draw)
    del bpy.types.WindowManager.ds_data
    bpy.app.handlers.load_pre.remove(load_pre_handler)
    #bpy.app.handlers.undo_pre.remove(undo_pre_handler)
    #bpy.app.handlers.undo_post.remove(undo_post_handler)
    #bpy.app.handlers.redo_pre.remove(undo_pre_handler)
    #bpy.app.handlers.redo_post.remove(undo_post_handler)
    bpy.app.handlers.save_pre.remove(undo_pre_handler)  
    bpy.app.handlers.save_post.remove(undo_post_handler)  
    del bpy.types.WindowManager.ds_keyframes

    # handle the keymap
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()

if __name__ == "__main__":
    register()           

    
